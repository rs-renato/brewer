package br.com.renatorodrigues.brewer.repository;

import br.com.renatorodrigues.brewer.model.Estilo;
import br.com.renatorodrigues.brewer.repository.helper.estilo.EstilosRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by renatorodrigues on 25/03/17.
 */
@Repository
public interface EstilosRepository extends JpaRepository<Estilo, Long>, EstilosRepositoryQueries {

    public Optional<Estilo> findByNomeIgnoreCase(String nome);
}
