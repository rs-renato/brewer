package br.com.renatorodrigues.brewer.converter;

import br.com.renatorodrigues.brewer.model.Estilo;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

/**
 * Created by renatorodrigues on 26/03/17.
 */
public class EstiloConverter implements Converter<String, Estilo> {

    @Override
    public Estilo convert(String codigo) {

        Estilo estilo = null;

        if(!StringUtils.isEmpty(codigo)){
            estilo = new Estilo();
            estilo.setCodigo(Long.valueOf(codigo));
        }

        return estilo;
    }
}
