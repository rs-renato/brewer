package br.com.renatorodrigues.brewer.service;

import br.com.renatorodrigues.brewer.exception.NomeCidadeJaCadastradaException;
import br.com.renatorodrigues.brewer.model.Cidade;
import br.com.renatorodrigues.brewer.repository.CidadesRepository;
import br.com.renatorodrigues.brewer.repository.filter.CidadeFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by renatorodrigues on 27/03/17.
 */
@Service
public class CidadeService {

    @Autowired
    private CidadesRepository cidadesRepository;
    
    public List<Cidade> findByEstadoCodigo(Long codigoEstado){
    	return cidadesRepository.findByEstadoCodigo(codigoEstado);
    }
	
    @Transactional
	public void salvar(Cidade cidade) throws NomeCidadeJaCadastradaException{
	
	    Optional<Cidade> cidadeExistente = cidadesRepository.findByNomeAndEstado(cidade.getNome(), cidade.getEstado());
	    if (cidadeExistente.isPresent()) {
		    throw new NomeCidadeJaCadastradaException("Nome de cidade já cadastrado");
	    }
	    
    	cidadesRepository.save(cidade);
	}
	
	public Page<Cidade> filtrar(CidadeFilter filter, Pageable pageable){
		return cidadesRepository.filtrar(filter, pageable);
	}
}
