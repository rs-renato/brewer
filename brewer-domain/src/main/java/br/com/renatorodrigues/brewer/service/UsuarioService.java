package br.com.renatorodrigues.brewer.service;

import br.com.renatorodrigues.brewer.exception.EmailJaCadastradaException;
import br.com.renatorodrigues.brewer.exception.SenhaObrigatoriaUsuarioException;
import br.com.renatorodrigues.brewer.model.Usuario;
import br.com.renatorodrigues.brewer.repository.UsuariosRepository;
import br.com.renatorodrigues.brewer.repository.filter.UsuarioFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

/**
 * Created by renatorodrigues on 27/03/17.
 */
@Service
public class UsuarioService {

    @Autowired
    private UsuariosRepository usuariosRepository;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
	
    @Transactional
	public void salvar(Usuario usuario) throws EmailJaCadastradaException, SenhaObrigatoriaUsuarioException{
	
	    Optional<Usuario> usuarioExistente = usuariosRepository.findByEmail(usuario.getEmail());
	    
	    if (usuarioExistente.isPresent() && !usuarioExistente.get().equals(usuario)) {
		    throw new EmailJaCadastradaException("Email de usuário já cadastrado");
	    }
	
	    if (usuario.isNovo() && StringUtils.isEmpty(usuario.getSenha())) {
		    throw new SenhaObrigatoriaUsuarioException("Senha é obrigatória para novo usuário");
	    }
	
		if (usuario.isNovo() || !StringUtils.isEmpty(usuario.getSenha())) {
			usuario.setSenha(this.passwordEncoder.encode(usuario.getSenha()));
		} else if (StringUtils.isEmpty(usuario.getSenha())) {
			usuario.setSenha(usuarioExistente.get().getSenha());
		}

		if (!usuario.isNovo() && usuario.getAtivo() == null) {
			usuario.setAtivo(usuarioExistente.get().getAtivo());
		}
	    
    	usuariosRepository.save(usuario);
	}
	
	public Optional<Usuario> porEmailEAtivo(String email){
    	return usuariosRepository.porEmailEAAtivo(email);
	}
	
	public List<String> permissoes(Usuario usuario){
		return usuariosRepository.permissoes(usuario);
	}
	
	public List<Usuario> findAll(){
		return usuariosRepository.findAll();
	}
	
	public Page<Usuario> filtrar(UsuarioFilter filter, Pageable pageable){
		return usuariosRepository.filtrar(filter, pageable);
	}
	
	@Transactional
	public void alterarStatus(Long[] codigos, StatusUsuario statusUsuario) {
		statusUsuario.atualizarStatus(codigos, usuariosRepository);
	}

	public Usuario buscarComGrupos(Long codigo) {
		return usuariosRepository.buscarComGrupos(codigo);
	}
}
