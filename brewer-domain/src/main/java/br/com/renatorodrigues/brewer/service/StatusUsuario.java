package br.com.renatorodrigues.brewer.service;


import br.com.renatorodrigues.brewer.repository.UsuariosRepository;

public enum StatusUsuario {

	ATIVAR {
		@Override
		public void atualizarStatus(Long[] codigos, UsuariosRepository usuariosRepository) {
			usuariosRepository.findByCodigoIn(codigos).forEach(u -> u.setAtivo(true));
		}
	},
	
	DESATIVAR {
		@Override
		public void atualizarStatus(Long[] codigos, UsuariosRepository usuarios) {
			usuarios.findByCodigoIn(codigos).forEach(u -> u.setAtivo(false));
		}
	};
	
	public abstract void atualizarStatus(Long[] codigos, UsuariosRepository usuariosRepository);
	
}
