package br.com.renatorodrigues.brewer.repository.helper.cerveja;

import br.com.renatorodrigues.brewer.dto.CervejaDTO;
import br.com.renatorodrigues.brewer.model.Cerveja;
import br.com.renatorodrigues.brewer.repository.filter.CervejaFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by renatorodrigues on 03/06/17.
 */
public interface CervejasRepositoryQueries {

	Page<Cerveja> filtrar(CervejaFilter filter, Pageable pageable);
	
	List<CervejaDTO> porSkuOuNome(String skuOuNome);
}
