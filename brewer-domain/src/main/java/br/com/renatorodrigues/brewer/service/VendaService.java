package br.com.renatorodrigues.brewer.service;

import br.com.renatorodrigues.brewer.model.StatusVenda;
import br.com.renatorodrigues.brewer.model.Venda;
import br.com.renatorodrigues.brewer.repository.VendasRepository;
import br.com.renatorodrigues.brewer.repository.filter.VendaFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * Created by renatorodrigues on 08/10/2017.
 */
@Service
public class VendaService {

	@Autowired
	private VendasRepository vendasRepository;
	
	@Transactional
	public Venda salvar(Venda venda){
		
		if (venda.isNova()){
			venda.setDataCriacao(LocalDateTime.now());
		}
		
		if (venda.getDataEntrega() != null){
			venda.setDataHoraEntrega(LocalDateTime.of(venda.getDataEntrega(),
					venda.getHorarioEntrega() != null ? venda.getHorarioEntrega() : LocalTime.NOON));
		}
		
		return vendasRepository.saveAndFlush(venda);
	}
	
	@Transactional
	public void emitir(Venda venda) {
		venda.setStatus(StatusVenda.EMITIDA);
		salvar(venda);
	}
	
	public Page<Venda> filtrar(VendaFilter vendaFilter, Pageable pageable) {
		return vendasRepository.filtrar(vendaFilter, pageable);
	}
}