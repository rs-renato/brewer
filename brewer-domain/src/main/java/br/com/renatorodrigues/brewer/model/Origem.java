package br.com.renatorodrigues.brewer.model;

/**
 * Created by renatorodrigues on 20/03/17.
 */
public enum Origem {

    NACIONAL("Nacional"),
    INTERNACIONAL("Internacional");

    private String descricao;

    Origem(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
