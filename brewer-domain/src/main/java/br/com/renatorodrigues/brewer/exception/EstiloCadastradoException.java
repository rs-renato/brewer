package br.com.renatorodrigues.brewer.exception;

/**
 * Created by renatorodrigues on 02/04/17.
 */
public class EstiloCadastradoException extends Exception {

    public EstiloCadastradoException(String message) {
        super(message);
    }
}
