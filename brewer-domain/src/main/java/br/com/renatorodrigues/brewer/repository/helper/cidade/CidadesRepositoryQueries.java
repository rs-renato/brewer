package br.com.renatorodrigues.brewer.repository.helper.cidade;

import br.com.renatorodrigues.brewer.model.Cidade;
import br.com.renatorodrigues.brewer.repository.filter.CidadeFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by renatorodrigues on 03/06/17.
 */
public interface CidadesRepositoryQueries {

	Page<Cidade> filtrar(CidadeFilter filter, Pageable pageable);
}
