package br.com.renatorodrigues.brewer.repository.helper.estilo;

import br.com.renatorodrigues.brewer.model.Estilo;
import br.com.renatorodrigues.brewer.repository.filter.EstiloFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by renatorodrigues on 11/06/17.
 */
public interface EstilosRepositoryQueries {
	
	Page<Estilo> filtrar(EstiloFilter filter, Pageable pageable);
}
