package br.com.renatorodrigues.brewer.repository.helper.cerveja;

import br.com.renatorodrigues.brewer.dto.CervejaDTO;
import br.com.renatorodrigues.brewer.model.Cerveja;
import br.com.renatorodrigues.brewer.repository.filter.CervejaFilter;
import br.com.renatorodrigues.brewer.repository.paginacao.PaginacaoUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by renatorodrigues on 03/06/17.
 */
public class CervejasRepositoryImpl implements CervejasRepositoryQueries {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@Override
	@Transactional(readOnly = true)
	public Page<Cerveja> filtrar(CervejaFilter filtro, Pageable pageable) {
		
		if (filtro == null){
			return null;
		}
		
		/*CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		
		CriteriaQuery<Cerveja> query = criteriaBuilder.createQuery(Cerveja.class);
		Root<Cerveja> root = query.from(Cerveja.class);
		
		criteriaBuilder.and(
				criteriaBuilder.equal(root.get("sku"), filtro.getSku())
		);*/
		
		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Cerveja.class);
		
		paginacaoUtil.preparar(criteria,pageable);
		
		adicionarFiltros(filtro, criteria);
		
		return new PageImpl<Cerveja>(criteria.list(),pageable, total(filtro));
	}
	
	@Override
	public List<CervejaDTO> porSkuOuNome(String skuOuNome) {
		
		String jpql = "select new br.com.renatorodrigues.brewer.dto.CervejaDTO(codigo, sku, nome, origem, valor, foto) "
				+ "from Cerveja where lower(sku) like :skuOuNome or lower(nome) like :skuOuNome";
		
		return entityManager.createQuery(jpql, CervejaDTO.class)
				.setParameter("skuOuNome",  "%" +skuOuNome.toLowerCase() + "%")
				.getResultList();
	}
	
	private Long total(CervejaFilter filtro) {
	
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Cerveja.class);
		adicionarFiltros(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltros(CervejaFilter filtro, Criteria criteria) {
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getSku())) {
				criteria.add(Restrictions.eq("sku", filtro.getSku()));
			}
			
			if (!StringUtils.isEmpty(filtro.getNome())) {
				criteria.add(Restrictions.ilike("nome", filtro.getNome(), MatchMode.ANYWHERE));
			}
			
			if (isEstiloPresente(filtro)) {
				criteria.add(Restrictions.eq("estilo", filtro.getEstilo()));
			}
			
			if (filtro.getSabor() != null) {
				criteria.add(Restrictions.eq("sabor", filtro.getSabor()));
			}
			
			if (filtro.getOrigem() != null) {
				criteria.add(Restrictions.eq("origem", filtro.getOrigem()));
			}
			
			if (filtro.getValorDe() != null) {
				criteria.add(Restrictions.ge("valor", filtro.getValorDe()));
			}
			
			if (filtro.getValorAte() != null) {
				criteria.add(Restrictions.le("valor", filtro.getValorAte()));
			}
		}
	}
	
	private boolean isEstiloPresente(CervejaFilter filtro) {
		return filtro.getEstilo() != null && filtro.getEstilo().getCodigo() != null;
	}
	
}
