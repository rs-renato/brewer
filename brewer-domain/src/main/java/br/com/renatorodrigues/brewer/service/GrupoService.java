package br.com.renatorodrigues.brewer.service;

import br.com.renatorodrigues.brewer.model.Grupo;
import br.com.renatorodrigues.brewer.repository.GruposRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by renatorodrigues on 27/03/17.
 */
@Service
public class GrupoService {

    @Autowired
    private GruposRepository gruposRepository;
    
    public List<Grupo> findAll(){
    	return  gruposRepository.findAll();
    }
}
