package br.com.renatorodrigues.brewer.exception;

/**
 * Created by renatorodrigues on 02/04/17.
 */
public class EmailJaCadastradaException extends Exception {

    public EmailJaCadastradaException(String message) {
        super(message);
    }
}
