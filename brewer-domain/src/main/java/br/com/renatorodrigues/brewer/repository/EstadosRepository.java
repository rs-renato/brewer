package br.com.renatorodrigues.brewer.repository;

import br.com.renatorodrigues.brewer.model.Estado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by renatorodrigues on 16/06/17.
 */
@Repository
public interface EstadosRepository extends JpaRepository<Estado, Long> {
}
