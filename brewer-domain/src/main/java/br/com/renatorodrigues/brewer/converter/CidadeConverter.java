package br.com.renatorodrigues.brewer.converter;

import br.com.renatorodrigues.brewer.model.Cidade;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

/**
 * Created by renatorodrigues on 26/03/17.
 */
public class CidadeConverter implements Converter<String, Cidade> {

    @Override
    public Cidade convert(String codigo) {

        Cidade cidade = null;

        if(!StringUtils.isEmpty(codigo)){
            cidade = new Cidade();
            cidade.setCodigo(Long.valueOf(codigo));
        }

        return cidade;
    }
}
