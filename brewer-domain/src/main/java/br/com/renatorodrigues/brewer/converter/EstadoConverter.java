package br.com.renatorodrigues.brewer.converter;

import br.com.renatorodrigues.brewer.model.Estado;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

/**
 * Created by renatorodrigues on 26/03/17.
 */
public class EstadoConverter implements Converter<String, Estado> {

    @Override
    public Estado convert(String codigo) {

        Estado estado = null;

        if(!StringUtils.isEmpty(codigo)){
            estado = new Estado();
            estado.setCodigo(Long.valueOf(codigo));
        }

        return estado;
    }
}
