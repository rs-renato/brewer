package br.com.renatorodrigues.brewer.repository;

import br.com.renatorodrigues.brewer.model.Venda;
import br.com.renatorodrigues.brewer.repository.helper.venda.VendasRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by renatorodrigues on 08/10/2017.
 */
public interface VendasRepository extends JpaRepository<Venda, Long>, VendasRepositoryQueries{
}