package br.com.renatorodrigues.brewer.repository.helper.usuario;

import br.com.renatorodrigues.brewer.model.Usuario;
import br.com.renatorodrigues.brewer.repository.filter.UsuarioFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Created by renatorodrigues on 01/08/17.
 */
public interface UsuariosRepositoryQueries {

	Optional<Usuario> porEmailEAAtivo(String email);
	
	List<String> permissoes(Usuario usuario);
	
	Page<Usuario> filtrar(UsuarioFilter filter, Pageable pageable);

	Usuario buscarComGrupos(Long codigo);
}
