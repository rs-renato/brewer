package br.com.renatorodrigues.brewer.exception;

/**
 * Created by renatorodrigues on 02/04/17.
 */
public class CpfCnpjClienteJaCadastradoException extends Exception {
	
    public CpfCnpjClienteJaCadastradoException(String message) {
        super(message);
    }
}
