package br.com.renatorodrigues.brewer.service;

import br.com.renatorodrigues.brewer.exception.EstiloCadastradoException;
import br.com.renatorodrigues.brewer.model.Estilo;
import br.com.renatorodrigues.brewer.repository.EstilosRepository;
import br.com.renatorodrigues.brewer.repository.filter.EstiloFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by renatorodrigues on 02/04/17.
 */
@Service
public class EstiloService {

    @Autowired
    private EstilosRepository estilosRepository;

    @Transactional
    public Estilo salvar(Estilo estilo) throws EstiloCadastradoException {

        String nome = estilo.getNome();

        if (estilosRepository.findByNomeIgnoreCase(nome).isPresent()){
            throw new EstiloCadastradoException(String.format("Estilo '%s' já cadastrado", nome));
        }

        return estilosRepository.saveAndFlush(estilo);
    }

    public List<Estilo> findAll(){
        return estilosRepository.findAll();
    }
	
	public Page<Estilo> filtrar(EstiloFilter estiloFilter, Pageable pageable) {
        return estilosRepository.filtrar(estiloFilter,pageable);
	}
}
