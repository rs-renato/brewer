package br.com.renatorodrigues.brewer.repository.filter;

import br.com.renatorodrigues.brewer.model.Estado;

/**
 * Created by renatorodrigues on 02/07/17.
 */
public class CidadeFilter {
	
	private Estado estado;
	private String nome;
	
	public Estado getEstado() {
		return estado;
	}
	
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
