package br.com.renatorodrigues.brewer.repository.helper.cliente;

import br.com.renatorodrigues.brewer.model.Cliente;
import br.com.renatorodrigues.brewer.repository.filter.ClienteFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by renatorodrigues on 03/06/17.
 */
public interface ClientesRepositoryQueries {

	Page<Cliente> filtrar(ClienteFilter filter, Pageable pageable);
}
