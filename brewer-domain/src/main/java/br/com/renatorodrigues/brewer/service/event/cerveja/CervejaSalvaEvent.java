package br.com.renatorodrigues.brewer.service.event.cerveja;

import br.com.renatorodrigues.brewer.model.Cerveja;
import org.springframework.util.StringUtils;

/**
 * Created by renatorodrigues on 24/05/17.
 */
public class CervejaSalvaEvent {
	
	private Cerveja cerveja;
	
	public CervejaSalvaEvent(Cerveja cerveja) {
		this.cerveja = cerveja;
	}
	
	public Cerveja getCerveja() {
		return cerveja;
	}
	
	 public boolean isFoto(){
		return !StringUtils.isEmpty(cerveja.getFoto());
	 }
	
	public boolean isNovaFoto() {
		return cerveja.isNovaFoto();
	}
}
