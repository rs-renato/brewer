package br.com.renatorodrigues.brewer.repository.helper.venda;

import br.com.renatorodrigues.brewer.model.Venda;
import br.com.renatorodrigues.brewer.repository.filter.VendaFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by renatorodrigues on 13/10/2017.
 */
public interface VendasRepositoryQueries {
	public Page<Venda> filtrar(VendaFilter filtro, Pageable pageable);
}
