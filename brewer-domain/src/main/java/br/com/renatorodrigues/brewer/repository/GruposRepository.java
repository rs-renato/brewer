package br.com.renatorodrigues.brewer.repository;

import br.com.renatorodrigues.brewer.model.Grupo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by renatorodrigues on 25/03/17.
 */
@Repository
public interface GruposRepository extends JpaRepository<Grupo, Long> {

}
