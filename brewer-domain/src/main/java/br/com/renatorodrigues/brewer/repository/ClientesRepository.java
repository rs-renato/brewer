package br.com.renatorodrigues.brewer.repository;

import br.com.renatorodrigues.brewer.model.Cliente;
import br.com.renatorodrigues.brewer.repository.helper.cliente.ClientesRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by renatorodrigues on 16/06/17.
 */
@Repository
public interface ClientesRepository extends JpaRepository<Cliente, Long>, ClientesRepositoryQueries{
	
	Optional<Cliente> findByCpfOuCnpj(String cpfOuCnpj);
	
	List<Cliente> findByNomeContainingIgnoreCase(String nome);
}