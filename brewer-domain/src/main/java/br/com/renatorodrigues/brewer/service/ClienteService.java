package br.com.renatorodrigues.brewer.service;

import br.com.renatorodrigues.brewer.exception.CpfCnpjClienteJaCadastradoException;
import br.com.renatorodrigues.brewer.model.Cliente;
import br.com.renatorodrigues.brewer.repository.ClientesRepository;
import br.com.renatorodrigues.brewer.repository.filter.ClienteFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by renatorodrigues on 27/03/17.
 */
@Service
public class ClienteService {

    @Autowired
    private ClientesRepository clientesRepository;
    
    @Transactional(rollbackFor = CpfCnpjClienteJaCadastradoException.class)
    public void save(Cliente cliente) throws CpfCnpjClienteJaCadastradoException {
	
	    Optional<Cliente> clienteExistente = clientesRepository.findByCpfOuCnpj(cliente.getCpfOuCnpjSemFormatacao());
	    
	    if (clienteExistente.isPresent()){
	    	throw new CpfCnpjClienteJaCadastradoException("CPF/CNPJ já cadastrado");
	    }
	    clientesRepository.saveAndFlush(cliente);
    }
	
	
	public Page<Cliente> filtrar(ClienteFilter clienteFilter, Pageable pageable) {
    	return clientesRepository.filtrar(clienteFilter, pageable);
	}
	
	public List<Cliente> findByNomeContainingIgnoreCase(String nome) {
    	return  clientesRepository.findByNomeContainingIgnoreCase(nome);
	}
}
