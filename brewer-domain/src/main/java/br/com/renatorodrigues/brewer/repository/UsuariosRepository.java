package br.com.renatorodrigues.brewer.repository;

import br.com.renatorodrigues.brewer.model.Usuario;
import br.com.renatorodrigues.brewer.repository.helper.usuario.UsuariosRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by renatorodrigues on 16/06/17.
 */
@Repository
public interface UsuariosRepository extends JpaRepository<Usuario, Long>, UsuariosRepositoryQueries{
	
	public Optional<Usuario> findByEmail(String email);
	
	List<Usuario> findByCodigoIn(Long[] codigos);
	
	/*@Query(value="select distinct p.nome from Usuario u inner join u.grupos g inner join g.permissoes p where u = :usuario" )
	public List<String> permissoes(@Param("usuario") Usuario usuario);*/
}
