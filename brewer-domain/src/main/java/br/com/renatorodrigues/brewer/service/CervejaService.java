package br.com.renatorodrigues.brewer.service;

import br.com.renatorodrigues.brewer.dto.CervejaDTO;
import br.com.renatorodrigues.brewer.exception.ImpossivelExcluirEntidadeException;
import br.com.renatorodrigues.brewer.model.Cerveja;
import br.com.renatorodrigues.brewer.repository.CervejasRepository;
import br.com.renatorodrigues.brewer.repository.filter.CervejaFilter;
import br.com.renatorodrigues.brewer.service.event.cerveja.CervejaExcluidaEvent;
import br.com.renatorodrigues.brewer.service.event.cerveja.CervejaSalvaEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import java.util.List;

/**
 * Created by renatorodrigues on 27/03/17.
 */
@Service
public class CervejaService {

    @Autowired
    private CervejasRepository cervejasRepository;

    @Autowired
    private ApplicationEventPublisher publisher;
	
    @Transactional
    public void salvar(Cerveja cerveja){
        cervejasRepository.save(cerveja);
        publisher.publishEvent(new CervejaSalvaEvent(cerveja));
    }
    
    public List<Cerveja> findAll(){
    	return cervejasRepository.findAll();
    }
	
	public Page<Cerveja> filtar(CervejaFilter filter, Pageable pageable){
		return cervejasRepository.filtrar(filter, pageable);
	}
	
	public List<CervejaDTO> porSkuOuNome(String skuOuNome){
		return cervejasRepository.porSkuOuNome(skuOuNome);
	}
	
	
	@Transactional
	public void excluir(Cerveja cerveja) {
		try {
			String foto = cerveja.getFoto();
			cervejasRepository.delete(cerveja);
			cervejasRepository.flush();
			publisher.publishEvent(new CervejaExcluidaEvent(foto));
		} catch (PersistenceException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar cerveja. Já foi usada em alguma venda.");
		}
	}
	
	
	/*public Object findAll(Specification specification) {
    	return cervejasRepository.findAll(specification);
	}*/
}
