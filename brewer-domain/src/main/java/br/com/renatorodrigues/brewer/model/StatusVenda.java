package br.com.renatorodrigues.brewer.model;

/**
 * Created by renatorodrigues on 08/10/2017.
 */
public enum StatusVenda {
	
	ORCAMENTO("Orçamento"),
	EMITIDA("Emitida"),
	CANCELADA("Cancelada");
	
	private String descricao;
	
	StatusVenda(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
