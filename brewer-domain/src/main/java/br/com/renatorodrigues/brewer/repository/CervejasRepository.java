package br.com.renatorodrigues.brewer.repository;

import br.com.renatorodrigues.brewer.model.Cerveja;
import br.com.renatorodrigues.brewer.repository.helper.cerveja.CervejasRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by renatorodrigues on 25/03/17.
 */
@Repository
public interface CervejasRepository extends JpaRepository<Cerveja, Long>, CervejasRepositoryQueries {

    public Optional<Cerveja> findBySkuIgnoreCase(String sku);
}
