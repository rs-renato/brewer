package br.com.renatorodrigues.brewer.validation;

import br.com.renatorodrigues.brewer.model.Cliente;
import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by renatorodrigues on 18/06/17.
 */
public class ClienteGroupSequenceProvider implements DefaultGroupSequenceProvider<Cliente> {
	
	@Override
	public List<Class<?>> getValidationGroups(Cliente cliente) {
		
		List<Class<?>> grupos = new ArrayList<>();
		//Classe Cliente deve fazer parte da redefinicao do grupo de validacao.
		// serve para os atributos que nao tiver 'groups'
		grupos.add(Cliente.class);
		
		if (isPessoaSelecionada(cliente)){
			grupos.add(cliente.getTipoPessoa().getGrupo());
		}
		
		return grupos;
	}
	
	private boolean isPessoaSelecionada(Cliente cliente) {
		return cliente != null && cliente.getTipoPessoa() != null;
	}
}
