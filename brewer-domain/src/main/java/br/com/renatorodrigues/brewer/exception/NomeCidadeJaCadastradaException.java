package br.com.renatorodrigues.brewer.exception;

/**
 * Created by renatorodrigues on 02/04/17.
 */
public class NomeCidadeJaCadastradaException extends Exception {

    public NomeCidadeJaCadastradaException(String message) {
        super(message);
    }
}
