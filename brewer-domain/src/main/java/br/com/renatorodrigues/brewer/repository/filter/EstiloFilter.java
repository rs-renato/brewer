package br.com.renatorodrigues.brewer.repository.filter;

/**
 * Created by renatorodrigues on 11/06/17.
 */
public class EstiloFilter {
	
	private String nome;
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
