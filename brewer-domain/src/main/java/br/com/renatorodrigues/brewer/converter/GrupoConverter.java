package br.com.renatorodrigues.brewer.converter;

import br.com.renatorodrigues.brewer.model.Grupo;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

/**
 * Created by renatorodrigues on 26/03/17.
 */
public class GrupoConverter implements Converter<String, Grupo> {

    @Override
    public Grupo convert(String codigo) {

        Grupo grupo = null;

        if(!StringUtils.isEmpty(codigo)){
            grupo = new Grupo();
            grupo.setCodigo(Long.valueOf(codigo));
        }

        return grupo;
    }
}
