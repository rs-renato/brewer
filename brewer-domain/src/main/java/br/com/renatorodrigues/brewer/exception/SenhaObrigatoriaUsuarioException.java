package br.com.renatorodrigues.brewer.exception;

/**
 * Created by renatorodrigues on 23/07/17.
 */
public class SenhaObrigatoriaUsuarioException extends Throwable {
	public SenhaObrigatoriaUsuarioException(String message) {
		super(message);
	}
	
}
