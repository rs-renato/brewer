package br.com.renatorodrigues.brewer.service;

import br.com.renatorodrigues.brewer.model.Estado;
import br.com.renatorodrigues.brewer.repository.EstadosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by renatorodrigues on 27/03/17.
 */
@Service
public class EstadoService {

    @Autowired
    private EstadosRepository estadosRepository;
    
    public List<Estado> findAll(){
    	return estadosRepository.findAll();
    }
}
