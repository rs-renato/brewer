package br.com.renatorodrigues.brewer.repository;

import br.com.renatorodrigues.brewer.model.Cidade;
import br.com.renatorodrigues.brewer.model.Estado;
import br.com.renatorodrigues.brewer.repository.helper.cidade.CidadesRepositoryQueries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by renatorodrigues on 16/06/17.
 */
@Repository
public interface CidadesRepository extends JpaRepository<Cidade, Long>, CidadesRepositoryQueries{
	public List<Cidade> findByEstadoCodigo(Long codigoEstado);
	public Optional<Cidade> findByNomeAndEstado(String nome, Estado estado);
}
