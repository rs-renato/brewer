package br.com.renatorodrigues.brewer.repository.helper.estilo;

import br.com.renatorodrigues.brewer.model.Estilo;
import br.com.renatorodrigues.brewer.repository.filter.EstiloFilter;
import br.com.renatorodrigues.brewer.repository.paginacao.PaginacaoUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by renatorodrigues on 11/06/17.
 */
public class EstilosRepositoryImpl implements EstilosRepositoryQueries {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@Override
	@Transactional(readOnly = true)
	public Page<Estilo> filtrar(EstiloFilter filter, Pageable pageable) {
		
		if (filter == null){
			return null;
		}
		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Estilo.class);
		
		paginacaoUtil.preparar(criteria,pageable);
		adicionarFiltros(filter, criteria);
		
		return new PageImpl<Estilo>(criteria.list(),pageable, total(filter));
	}
	
	private Long total(EstiloFilter filtro) {
		
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Estilo.class);
		adicionarFiltros(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltros(EstiloFilter filter, Criteria criteria) {
		
		if (!StringUtils.isEmpty(filter.getNome())) {
			criteria.add(Restrictions.ilike("nome", filter.getNome(), MatchMode.ANYWHERE));
		}
	}
}
