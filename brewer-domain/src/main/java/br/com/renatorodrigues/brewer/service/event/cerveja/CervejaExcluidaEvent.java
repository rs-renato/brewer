package br.com.renatorodrigues.brewer.service.event.cerveja;

/**
 * Created by renatorodrigues on 24/05/17.
 */
public class CervejaExcluidaEvent {
	
	private String foto;
	
	public CervejaExcluidaEvent(String foto) {
		this.foto = foto;
	}
	
	public String getFoto() {
		return foto;
	}
}
