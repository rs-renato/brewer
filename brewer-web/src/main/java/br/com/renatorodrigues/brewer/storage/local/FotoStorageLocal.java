package br.com.renatorodrigues.brewer.storage.local;

import br.com.renatorodrigues.brewer.storage.FotoStorage;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.UUID;

/**
 * Created by renatorodrigues on 11/04/17.
 */
public class FotoStorageLocal implements FotoStorage {

    private static final Logger logger = LogManager.getLogger(FotoStorageLocal.class);
	private static final String THUMBNAIL_PREFIX = "thumbnail.";
	

    private Path local;
    private Path temp;
	
	public FotoStorageLocal() {
		this(FileSystems.getDefault().getPath(System.getenv("HOME"), ".brewerfotos"));
    }
	
	public FotoStorageLocal(Path local) {
		this.local = local;
		criarPastas();
	}
	
	@Override
	public String saveTemp(MultipartFile[] files) {
		
		String nomeNome = null;
		
		logger.info("Salvando foto temporariamente...");
		
		if(files != null && files.length > 0){
			
			MultipartFile file = files[0];
			
			 nomeNome = renomearArquivo(file.getOriginalFilename());
			
			String path = this.temp.toAbsolutePath().toString() + File.separator + nomeNome;
			
			try {
				file.transferTo(new File(path));
			} catch (IOException e) {
				throw new RuntimeException("Erro ao salvar arquivo na pasta temporária",e);
			}
		}
		
		return nomeNome;
	}
	
	@Override
	public void save(String foto) {
		
		try {
			
			Path localFoto = local.resolve(foto);
			
			Files.move(temp.resolve(foto), localFoto);
			
			Thumbnails.of(localFoto.toString())
					.size(40,68)
					.toFiles(Rename.PREFIX_DOT_THUMBNAIL);
			
		} catch (IOException e) {
			throw new RuntimeException("Erro ao salvar arquivo na pasta final",e);
		}
	}
	
	@Override
	public byte[] download(String name, boolean isTemp) throws NoSuchFileException {
		
		try {
			return Files.readAllBytes(!isTemp ? local.resolve(name) : temp.resolve(name));
		} catch (NoSuchFileException e){
			throw e;
		} catch (IOException e) {
			throw new RuntimeException(String.format("Erro ao baixar arquivo da pasta %s.", isTemp ? "temporária":"",e));
		}
	}
	
	@Override
	public byte[] downloadThumbnail(String fotoCerveja) throws NoSuchFileException{
		return download(THUMBNAIL_PREFIX + fotoCerveja, false);
	}
	
	@Override
	public void excluir(String foto) {
		try {
			Files.deleteIfExists(this.local.resolve(foto));
			Files.deleteIfExists(this.local.resolve(THUMBNAIL_PREFIX + foto));
		} catch (IOException e) {
			logger.warn(String.format("Erro apagando foto '%s'. Mensagem: %s", foto, e.getMessage()));
		}
		
	}
	
	private void criarPastas() {
		
		try {
			
			Files.createDirectories(this.local);
			this.temp = FileSystems.getDefault().getPath(this.local.toString(), "temp");
			Files.createDirectories(this.temp);
			
			logger.info("Pastas criadas para salvar foto..");
			logger.info("Pasta default: " + this.local.toAbsolutePath());
			logger.info("Pasta temp: " + this.temp.toAbsolutePath());
			
		} catch (IOException e) {
			throw new RuntimeException("Erro ao criar pasta para salvar foto",e);
		}
	}
	
	private String renomearArquivo(String nome){
		return UUID.randomUUID().toString() + "_" + nome;
	}
}
