package br.com.renatorodrigues.brewer.thymeleaf.processor;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

/**
 * Created by renatorodrigues on 09/04/17.
 */
public class MessageElementTagProcessor extends AbstractElementTagProcessor{

    private static final String TAG_NAME = "messages";
    private static final int PRECEDENCE = 1000;

    public MessageElementTagProcessor(String dialectPrefix){
        super(TemplateMode.HTML, dialectPrefix, TAG_NAME, true, null, false, PRECEDENCE);
    }

    @Override
    protected void doProcess(ITemplateContext context, IProcessableElementTag tag, IElementTagStructureHandler structureHandler) {

        IModelFactory modelFactory = context.getModelFactory();

        IModel iModel = modelFactory.createModel();

        iModel.add(modelFactory.createStandaloneElementTag("th:block", "th:replace", "fragments/MensagensSucesso :: success"));
        iModel.add(modelFactory.createStandaloneElementTag("th:block", "th:replace", "fragments/MensagensErro :: error"));

        structureHandler.replaceWith(iModel,true);
    }
}
