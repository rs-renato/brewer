package br.com.renatorodrigues.brewer.controller;


import br.com.renatorodrigues.brewer.exception.CpfCnpjClienteJaCadastradoException;
import br.com.renatorodrigues.brewer.model.Cliente;
import br.com.renatorodrigues.brewer.model.TipoPessoa;
import br.com.renatorodrigues.brewer.page.PageWrapper;
import br.com.renatorodrigues.brewer.repository.filter.ClienteFilter;
import br.com.renatorodrigues.brewer.service.ClienteService;
import br.com.renatorodrigues.brewer.service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/clientes")
public class ClientesController {
	
	@Autowired
	private EstadoService estadoService;
	
	@Autowired
	private ClienteService clienteService;

    @GetMapping("/novo")
    public ModelAndView novo(Cliente cliente){
	    ModelAndView mv = new ModelAndView("cliente/CadastroCliente");
	    mv.addObject("tiposPessoa", TipoPessoa.values());
	    mv.addObject("estados", estadoService.findAll());
        return mv;
    }
    
    @PostMapping("/novo")
	public ModelAndView save(@Valid Cliente cliente, BindingResult result, RedirectAttributes attributes){
    	
    	if (result.hasErrors()){
    		return novo(cliente);
	    }
    	
	    try {
	        clienteService.save(cliente);
	    }catch (CpfCnpjClienteJaCadastradoException e){
		    result.rejectValue("cpfOuCnpj", e.getMessage(), e.getMessage());
    	    return novo(cliente);
	    }
    	attributes.addFlashAttribute("mensagem", "Cliente salvo com sucesso!");
    	return new ModelAndView("redirect:/clientes/novo");
    }
    
    @GetMapping
	public ModelAndView pesquisar(ClienteFilter clienteFilter, BindingResult result,
                                  @PageableDefault Pageable pageable, HttpServletRequest request){
		
		ModelAndView mv = new ModelAndView("cliente/PesquisaClientes");
	
	    Page<Cliente> page = clienteService.filtrar(clienteFilter, pageable);
	    PageWrapper<Cliente> pagina = new PageWrapper<>(page, request);
	
	    mv.addObject("pagina", pagina);
		
		return mv;
    }
    
    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody  List<Cliente> pesquisar(String nome){
		
		if (StringUtils.isEmpty(nome) || nome.length() < 3){
			throw new IllegalArgumentException();
		}
		
		return  clienteService.findByNomeContainingIgnoreCase(nome);
    }
    
    @ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Void> handleIllegalArgumentException(IllegalArgumentException e){
    	return ResponseEntity.badRequest().build();
    }
}
