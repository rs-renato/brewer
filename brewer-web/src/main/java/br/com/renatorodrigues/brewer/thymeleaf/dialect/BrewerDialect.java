package br.com.renatorodrigues.brewer.thymeleaf.dialect;

import br.com.renatorodrigues.brewer.thymeleaf.processor.*;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by renatorodrigues on 04/04/17.
 */
public class BrewerDialect extends AbstractProcessorDialect {

    public BrewerDialect() {
        super("Brewer Dialect", "brewer", 10);
    }

    @Override
    public Set<IProcessor> getProcessors(String dialectPrefix) {

        Set<IProcessor> set = new HashSet<>();
        set.add(new ClassForErrorAttributeTagProcessor(dialectPrefix));
        set.add(new MessageElementTagProcessor(dialectPrefix));
        set.add(new OrdenacaoElementTagProcessor(dialectPrefix));
        set.add(new PaginacaoElementTagProcessor(dialectPrefix));
        set.add(new MenuAttributeTagProcessor(dialectPrefix));

        return set;
    }
}
