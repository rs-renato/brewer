package br.com.renatorodrigues.brewer.specification;

import br.com.renatorodrigues.brewer.repository.filter.CervejaFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created by renatorodrigues on 02/06/17.
 */
public class CervejaFilterSpecification implements Specification<CervejaFilter> {
	
	
	/*public static Specification<CervejaFilter> skuIs(String sku){
		return ((root, query, cb) -> (cb.equal(root.get("sku"), sku)));
	}*/
	
	private CervejaFilter cervejaFilter;
	
	public CervejaFilterSpecification(CervejaFilter cervejaFilter) {
		this.cervejaFilter = cervejaFilter;
	}
	
	@Override
	public Predicate toPredicate(Root<CervejaFilter> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
		
		return  criteriaBuilder.and(
					criteriaBuilder.equal(root.get("nome"), cervejaFilter.getNome()),
					criteriaBuilder.equal(root.get("sku"), cervejaFilter.getSku()));
	}
}
