package br.com.renatorodrigues.brewer.storage;

import org.springframework.web.multipart.MultipartFile;

import java.nio.file.NoSuchFileException;

/**
 * Created by renatorodrigues on 13/04/17.
 */
public interface FotoStorage {
	
	String saveTemp(MultipartFile[] files);
	void save(String foto);
	void excluir(String foto);
	byte[] download(String name, boolean isTemp) throws NoSuchFileException;
	byte[] downloadThumbnail(String fotoCerveja) throws NoSuchFileException;
}
