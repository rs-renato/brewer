package br.com.renatorodrigues.brewer.config.init;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.SessionTrackingMode;
import java.util.EnumSet;

/**
 * Created by renatorodrigues on 01/08/17.
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
	
	@Override
	protected void beforeSpringSecurityFilterChain(ServletContext servletContext) {
		
//		servletContext.getSessionCookieConfig().setMaxAge(20);
		
		//avoid url JSESSION id URL rewritten, and save cookies
		servletContext.setSessionTrackingModes(EnumSet.of(SessionTrackingMode.COOKIE));
		
		FilterRegistration.Dynamic encodingFilter = servletContext.addFilter(
				"encodingFilter", new CharacterEncodingFilter());
		
		encodingFilter.setInitParameter("encoding","UTF-8");
		encodingFilter.setInitParameter("forceEncoding","true");
		encodingFilter.addMappingForUrlPatterns(null,false, "/*");

	}
	
}
