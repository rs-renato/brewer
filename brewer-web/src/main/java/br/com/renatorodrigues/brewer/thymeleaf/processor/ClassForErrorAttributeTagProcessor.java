package br.com.renatorodrigues.brewer.thymeleaf.processor;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.spring4.util.FieldUtils;
import org.thymeleaf.templatemode.TemplateMode;

/**
 * Created by renatorodrigues on 04/04/17.
 */
public class ClassForErrorAttributeTagProcessor extends AbstractAttributeTagProcessor {

    private static final String ATTRIBUTE = "classforerror";
    private static final int PRECEDENCE = 10;

    public ClassForErrorAttributeTagProcessor(String dialectPrefix){
        super(TemplateMode.HTML, dialectPrefix, null, false, ATTRIBUTE, true, PRECEDENCE, true);
    }


    @Override
    protected void doProcess(ITemplateContext context, IProcessableElementTag tag, AttributeName attributeName,
                             String attributeValue, IElementTagStructureHandler structureHandler) {

        boolean hasError = FieldUtils.hasErrors(context, attributeValue);

        if (hasError){
            structureHandler.setAttribute("class", tag.getAttributeValue("class") + " has-error");
        }
    }
}
