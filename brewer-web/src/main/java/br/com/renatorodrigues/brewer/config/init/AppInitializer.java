package br.com.renatorodrigues.brewer.config.init;

import br.com.renatorodrigues.brewer.config.*;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.filter.HttpPutFormContentFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;

/**
 * Classe responsável por iniciar as configurações da aplicação
 */
public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[]{JPAConfig.class, ServiceConfig.class, SecurityConfig.class};
	}

    /**
     * Retorna a classe que instrui o Dispatcher a
     * localizar os Controllers.
     * @return
     */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[]{WebConfig.class, MailConfig.class};
	}

    /**
     * Método responsável por definir o padrão de URL
     * que será delegado para o DispatcherServlet. "/"
     * significa que tudo após a / será entregue ao Dispatcher
     * (qualquer URL dentro da aplicação).
     *
     * <servlet-mapping>
     *	 <url-pattern>/</url-pattern>
     * </servlet-mapping>
     * @return
     */
	@Override
	protected String[] getServletMappings() {
		return new String[]{"/"};
	}
	

    @Override
    protected Filter[] getServletFilters() {
	
	    OpenEntityManagerInViewFilter openEntityManagerInViewFilter = new OpenEntityManagerInViewFilter();
	    openEntityManagerInViewFilter.setEntityManagerFactoryBeanName("entityManagerFactory");
	
	    return new Filter[]{openEntityManagerInViewFilter, new HttpPutFormContentFilter()};
    }
}
