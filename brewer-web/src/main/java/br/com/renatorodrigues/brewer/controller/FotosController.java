package br.com.renatorodrigues.brewer.controller;

import br.com.renatorodrigues.brewer.dto.FotoDTO;
import br.com.renatorodrigues.brewer.storage.FotoStorage;
import br.com.renatorodrigues.brewer.storage.FotoStorageRunnable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.NoSuchFileException;

/**
 * Created by renatorodrigues on 09/04/17.
 */
@RestController
@RequestMapping("/fotos")
public class FotosController {

    private Logger logger = LogManager.getLogger(FotosController.class);
	
	@Autowired
	private FotoStorage fotoStorage;
	
    @PostMapping(value = "/upload",produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody DeferredResult<FotoDTO> upload(@RequestParam("files[]") MultipartFile[] files) {
		
        DeferredResult<FotoDTO> resultado = new DeferredResult<>();

	    new Thread(new FotoStorageRunnable(files, resultado, fotoStorage)).start();

        return resultado;
    }
    
    @GetMapping("/download/{name:.*\\.(?:jpg|jpeg|png)$}")
    public byte[] download(@PathVariable String name){
	    try {
		    return fotoStorage.download(name, false);
	    } catch (NoSuchFileException e) {
		    logger.error("Foto não encontrada!");
	    }
	    return null;
    }
	
	@GetMapping(value="/download/temp/{name:.*\\.(?:jpg|jpeg|png)$}")
	public byte[] downloadTemp(@PathVariable String name){
		try {
			return fotoStorage.download(name, true);
		} catch (NoSuchFileException e) {
			logger.error("Foto temporária não encontrada!");
		}
		return null;
	}

}
