package br.com.renatorodrigues.brewer.config;

import br.com.renatorodrigues.brewer.storage.FotoStorage;
import br.com.renatorodrigues.brewer.storage.local.FotoStorageLocal;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by renatorodrigues on 08/04/17.
 */
@Configuration
@ComponentScan(basePackages = {"br.com.renatorodrigues.brewer.service"})
public class ServiceConfig {
	
	@Bean
	public FotoStorage storageLocal(){
		return new FotoStorageLocal();
	}
}
