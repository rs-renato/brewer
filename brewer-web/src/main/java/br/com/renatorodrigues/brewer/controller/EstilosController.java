package br.com.renatorodrigues.brewer.controller;


import br.com.renatorodrigues.brewer.exception.EstiloCadastradoException;
import br.com.renatorodrigues.brewer.model.Estilo;
import br.com.renatorodrigues.brewer.page.PageWrapper;
import br.com.renatorodrigues.brewer.repository.filter.EstiloFilter;
import br.com.renatorodrigues.brewer.service.EstiloService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/estilos")
public class EstilosController {

    private Logger logger = LogManager.getLogger(EstilosController.class);

    @Autowired
    private EstiloService estiloService;

    @GetMapping("/novo")
    public ModelAndView novo(Estilo estilo){

        logger.info("Carregando formulário de estilo...");

        return new ModelAndView("estilo/CadastroEstilo");
    }

    @PostMapping("/novo")
    public ModelAndView cadastrar(@Valid Estilo estilo, BindingResult result, RedirectAttributes attributes){

        if (result.hasErrors()){
            return novo(estilo);
        }

        try {
            estiloService.salvar(estilo);
        } catch (EstiloCadastradoException e) {
            logger.error(e.getMessage());
            result.reject("nome", e.getMessage());
            return novo(estilo);
        }

        logger.info("Estilo salvo com sucesso...");

        attributes.addFlashAttribute("mensagem", "Estilo salvo csom sucesso");

        return new ModelAndView("redirect:/estilos/novo");
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody ResponseEntity<?> cadastrar(@RequestBody @Valid Estilo estilo, BindingResult result){

        if (result.hasErrors()){
            return ResponseEntity.badRequest().body(result.getFieldError("nome").getDefaultMessage());
        }

        try {
            estilo = estiloService.salvar(estilo);
        } catch (EstiloCadastradoException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.ok(estilo);
    }
	
	@GetMapping
	public ModelAndView pesquisar(EstiloFilter estiloFilter, BindingResult result ,
	                              @PageableDefault(size = 2) Pageable pageable, HttpServletRequest request) {
    	
		ModelAndView mv = new ModelAndView("estilo/PesquisaEstilos");
		
		Page<Estilo> page = estiloService.filtrar(estiloFilter, pageable);
		PageWrapper<Estilo> pagina = new PageWrapper<>(page, request);
		
		mv.addObject("pagina", pagina);
		return mv;
	}
}
