package br.com.renatorodrigues.brewer.thymeleaf.processor;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IAttribute;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

/**
 * Created by renatorodrigues on 11/06/17.
 */
public class PaginacaoElementTagProcessor extends AbstractElementTagProcessor {
	
	private static final String TAG_NAME = "pagination";
	private static final int PRECEDENCE = 1000;
	
	public PaginacaoElementTagProcessor(String dialectPrefix) {
		super(TemplateMode.HTML, dialectPrefix, TAG_NAME, true, null, false, PRECEDENCE);
	}
	
	@Override
	protected void doProcess(ITemplateContext context, IProcessableElementTag tag, IElementTagStructureHandler structureHandler) {
		
		IModelFactory iModelFactory = context.getModelFactory();
		
		IModel iModel = iModelFactory.createModel();
		
		IAttribute page = tag.getAttribute("page");
		
		iModel.add(iModelFactory.createStandaloneElementTag(
				"th:block",
				"th:replace",
				String.format("fragments/Paginacao :: pagination (%s)",page.getValue())));
		
		structureHandler.replaceWith(iModel,true);
	}
}
