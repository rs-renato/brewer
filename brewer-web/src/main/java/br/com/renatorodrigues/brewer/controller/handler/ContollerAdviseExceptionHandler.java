package br.com.renatorodrigues.brewer.controller.handler;

import br.com.renatorodrigues.brewer.exception.EstiloCadastradoException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by renatorodrigues on 03/04/17.
 */
//@ControllerAdvice
public class ContollerAdviseExceptionHandler {

    @ExceptionHandler(EstiloCadastradoException.class)
    public ResponseEntity<String> handleEstiloCadastradoException(EstiloCadastradoException ex){
        return ResponseEntity.badRequest().body(ex.getMessage());
    }
}
