package br.com.renatorodrigues.brewer.thymeleaf.processor;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IAttribute;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

/**
 * Created by renatorodrigues on 07/06/17.
 */
public class OrdenacaoElementTagProcessor  extends AbstractElementTagProcessor{
	
	private static final String TAG_NAME = "order";
	private static final int PRECEDENCE = 1000;
	
	public OrdenacaoElementTagProcessor(String dialectPrefix) {
		super(TemplateMode.HTML, dialectPrefix, TAG_NAME, true, null, false, PRECEDENCE);
	}
	
	@Override
	protected void doProcess(ITemplateContext context, IProcessableElementTag tag, IElementTagStructureHandler structureHandler) {
		
		IModelFactory modelFactory = context.getModelFactory();
		
		IModel iModel = modelFactory.createModel();
		
		IAttribute page = tag.getAttribute("page");
		IAttribute field = tag.getAttribute("field");
		IAttribute text = tag.getAttribute("text");
		
		//th:block th:replace="fragments/Ordenacao :: order (${pagina}, 'sku', 'SKU')"></th:block>-->
		
		iModel.add(modelFactory.createStandaloneElementTag(
				"th:block",
				"th:replace",
				String.format("fragments/Ordenacao :: order (%s, %s, '%s')", page.getValue(), field.getValue(), text.getValue())));
		
		structureHandler.replaceWith(iModel,true);
	}
}
