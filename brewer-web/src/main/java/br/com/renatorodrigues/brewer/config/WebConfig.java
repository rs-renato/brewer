package br.com.renatorodrigues.brewer.config;

import br.com.renatorodrigues.brewer.converter.CidadeConverter;
import br.com.renatorodrigues.brewer.converter.EstadoConverter;
import br.com.renatorodrigues.brewer.converter.EstiloConverter;
import br.com.renatorodrigues.brewer.converter.GrupoConverter;
import br.com.renatorodrigues.brewer.thymeleaf.dialect.BrewerDialect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.github.mxab.thymeleaf.extras.dataattribute.dialect.DataAttributeDialect;
import com.google.common.cache.CacheBuilder;
import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.beans.BeansException;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.support.DomainClassConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.format.number.NumberStyleFormatter;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;

import javax.servlet.MultipartConfigElement;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Classe de configuração que instrui o dispatcher a encontrar
 * os controllers e os view resolvers
 */
@Configuration
@EnableSpringDataWebSupport
@ComponentScan(basePackages = {"br.com.renatorodrigues.brewer.controller","br.com.renatorodrigues.brewer.session"})
@EnableWebMvc
@EnableCaching
@EnableAsync
public class WebConfig extends WebMvcConfigurerAdapter implements ApplicationContextAware{

	private ApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	@Bean
	public ViewResolver createViewResolver(){
		
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setTemplateEngine(createTemplateEngine());
		viewResolver.setCharacterEncoding("UTF-8");
		
		return viewResolver;
	}
	
	
	@Bean
	public TemplateEngine createTemplateEngine(){
		
		SpringTemplateEngine engine = new SpringTemplateEngine();
		engine.setEnableSpringELCompiler(true);
		engine.setTemplateResolver(createTemplateResolver());

        //thymeleaf layout dialect
        engine.addDialect(new LayoutDialect());
        engine.addDialect(new BrewerDialect());
        engine.addDialect(new DataAttributeDialect());
        engine.addDialect(new SpringSecurityDialect());

		return engine;
	}

    /**
     * Método que cria o template resolver da aplicação
     * @return
     */
	private ITemplateResolver createTemplateResolver(){
		
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		
		resolver.setApplicationContext(applicationContext);
		resolver.setTemplateMode(TemplateMode.HTML);
		resolver.setPrefix("classpath:/templates/");
		resolver.setSuffix(".html");
		
		resolver.setCacheable(false);
		
		return resolver;
	}

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/");

    }
	
	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver();
		resolver.setOneIndexedParameters(true);
		resolver.setFallbackPageable(new PageRequest(1, 5));
		argumentResolvers.add(resolver);
	}
	
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(mappingJackson2HttpMessageConverter());
		converters.add(byteArrayHttpMessageConverter());
		super.configureMessageConverters(converters);
	}
	
	@Bean
	public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter(){
		return new ByteArrayHttpMessageConverter();
	}
	
	@Bean
	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
		
		ObjectMapper mapper = new ObjectMapper();
		//Registering Hibernate5Module to support lazy objects
		mapper.registerModule(new Hibernate5Module());
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		
		return new MappingJackson2HttpMessageConverter(mapper);
	}
	
	@Bean
    public FormattingConversionService mvcConversionService(){

        DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService();
        conversionService.addConverter(new EstiloConverter());
        conversionService.addConverter(new CidadeConverter());
        conversionService.addConverter(new EstadoConverter());
        conversionService.addConverter(new GrupoConverter());

        NumberStyleFormatter bigDecimalFormatter = new NumberStyleFormatter("#,##0.00");
        NumberStyleFormatter integerFormatter = new NumberStyleFormatter("#,##0");

        conversionService.addFormatterForFieldType(BigDecimal.class, bigDecimalFormatter);
        conversionService.addFormatterForFieldType(Integer.class, integerFormatter);
		
		
		DateTimeFormatterRegistrar dateTimeFormatter = new DateTimeFormatterRegistrar();
		dateTimeFormatter.setDateFormatter(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		dateTimeFormatter.setTimeFormatter(DateTimeFormatter.ofPattern("HH:mm"));
		dateTimeFormatter.registerFormatters(conversionService);

        return conversionService;
    }

    @Bean
    public LocaleResolver localeResolver(){
        return new FixedLocaleResolver(new Locale("pt","BR"));
    }

    @Bean
    protected MultipartConfigElement multipartConfigElement() {
        return new MultipartConfigElement("");
    }

    @Bean
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(1 * 1024 * 1024);//1MB
        return multipartResolver;
    }
    
    @Bean
	public CacheManager cacheManager(){
	
	    CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder()
			    .maximumSize(10)
			    .expireAfterAccess(1, TimeUnit.HOURS);
	
	    GuavaCacheManager cacheManager = new GuavaCacheManager();
	    cacheManager.setCacheBuilder(cacheBuilder);
    	
        return cacheManager;
    }
    
    @Bean
	public MessageSource messageSource(){
	
	    ReloadableResourceBundleMessageSource bundle = new ReloadableResourceBundleMessageSource();
	    bundle.setBasename("classpath:/messages");
	    bundle.setDefaultEncoding("UTF-8"); // http://www.utf8-chartable.de/
	    
	    return bundle;
    }
    
    @Bean
	public DomainClassConverter<FormattingConversionService> domainClassConverter(){
		return new DomainClassConverter<>(mvcConversionService());
    }
}
