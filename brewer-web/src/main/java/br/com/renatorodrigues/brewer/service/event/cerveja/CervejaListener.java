package br.com.renatorodrigues.brewer.service.event.cerveja;

import br.com.renatorodrigues.brewer.storage.FotoStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Created by renatorodrigues on 24/05/17.
 */
@Component
public class CervejaListener {

	@Autowired
	private FotoStorage fotoStorage;
	
	@EventListener(condition = "#event.foto and #event.novaFoto")
	public void cervejaSalva(CervejaSalvaEvent event){
		fotoStorage.save(event.getCerveja().getFoto());
	}
	
	@EventListener
	public void cervejaExcluida(CervejaExcluidaEvent event){
		fotoStorage.excluir(event.getFoto());
	}
}
