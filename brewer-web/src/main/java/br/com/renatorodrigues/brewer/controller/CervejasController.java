package br.com.renatorodrigues.brewer.controller;

import br.com.renatorodrigues.brewer.dto.CervejaDTO;
import br.com.renatorodrigues.brewer.exception.ImpossivelExcluirEntidadeException;
import br.com.renatorodrigues.brewer.model.Cerveja;
import br.com.renatorodrigues.brewer.model.Origem;
import br.com.renatorodrigues.brewer.model.Sabor;
import br.com.renatorodrigues.brewer.page.PageWrapper;
import br.com.renatorodrigues.brewer.repository.filter.CervejaFilter;
import br.com.renatorodrigues.brewer.service.CervejaService;
import br.com.renatorodrigues.brewer.service.EstiloService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/cervejas")
public class CervejasController {

    private Logger logger = LogManager.getLogger(CervejasController.class);

    @Autowired
    private EstiloService estiloService;

    @Autowired
    private CervejaService cervejaService;
	
    @GetMapping("/nova")
    public ModelAndView nova(Cerveja cerveja) {

        logger.info("Carregando formulário de cerveja...");

        ModelAndView mv = new ModelAndView("cerveja/CadastroCerveja");
        mv.addObject("sabores", Sabor.values());
        mv.addObject("origens", Origem.values());
        mv.addObject("estilos", estiloService.findAll());

        return mv;
    }

    @PostMapping({"/nova","{\\d+}"})
    public ModelAndView salvar(@Valid Cerveja cerveja, BindingResult result, RedirectAttributes attributes) {

        if (result.hasErrors()) {
            return nova(cerveja);
        }

        cervejaService.salvar(cerveja);

        logger.info("Cerveja salva com sucesso...");

        attributes.addFlashAttribute("mensagem", "Cerveja salva com sucesso!");
        return new ModelAndView("redirect:/cervejas/nova");
    }
    
    @GetMapping
    public ModelAndView pesquisar(CervejaFilter cervejaFilter, BindingResult result,
                                  @PageableDefault Pageable pageable, HttpServletRequest request){
    
    	ModelAndView mv = new ModelAndView("cerveja/PesquisaCervejas");
	    mv.addObject("estilos", estiloService.findAll());
//	    mv.addObject("cervejas", cervejaService.findAll(new CervejaFilterSpecification(cervejaFilter)));
	    Page<Cerveja> page = cervejaService.filtar(cervejaFilter, pageable);
	    PageWrapper<Cerveja> pagina = new PageWrapper<>(page, request);
	    mv.addObject("pagina", pagina);
	    mv.addObject("sabores", Sabor.values());
	    mv.addObject("origens", Origem.values());
	    
    	return mv;
    }
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Cerveja cerveja) {
		try {
			cervejaService.excluir(cerveja);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable("codigo") Cerveja cerveja){
		
		ModelAndView mv = nova(cerveja);
		mv.addObject(cerveja);
    	
        return  mv;
	}
    
    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<CervejaDTO> pesquisar(String skuOuNome){
        return cervejaService.porSkuOuNome(skuOuNome);
    }
}