package br.com.renatorodrigues.brewer.storage;

import br.com.renatorodrigues.brewer.dto.FotoDTO;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by renatorodrigues on 10/04/17.
 */
public class FotoStorageRunnable implements Runnable {
	
	private final FotoStorage fotoStorage;
	private MultipartFile[] files;
    private DeferredResult<FotoDTO> resultado;

    public FotoStorageRunnable(MultipartFile[] files, DeferredResult<FotoDTO> resultado, FotoStorage fotoStorage) {
        this.files = files;
        this.resultado = resultado;
        this.fotoStorage = fotoStorage;
    }

    @Override
    public void run() {

        MultipartFile file = files[0];

        String contentType = file.getContentType();
        
        String nome = fotoStorage.saveTemp(files);

        resultado.setResult(new FotoDTO(nome, contentType));
    }
}
