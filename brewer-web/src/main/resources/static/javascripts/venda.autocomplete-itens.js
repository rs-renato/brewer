Brewer = Brewer || {};

Brewer.Autocomplete = (function() {

    function Autocomplete() {
        this.skuOuNomeInput = $('.js-sku-nome-cerveja-input');
        this.template = Handlebars.compile($('#template-autocomplete-cerveja').html());

        //event emitter
        this.emitter = $({});
        this.on = this.emitter.on.bind(this.emitter);
    }

    Autocomplete.prototype.iniciar = function(){
        var options = {
            url : function(skuOuNome){
                return this.skuOuNomeInput.data('url') + '?skuOuNome=' + skuOuNome;
            }.bind(this),
            getValue: 'nome',
            minCharNumber: 3,
            requestDelay: 350,
            ajaxSettings: {
                contentType: 'application/json'
            },
            template: {
                type: 'custom',
                method: applyTableTemplate.bind(this)
            },
            list:{
                onChooseEvent: onItemSelecionado.bind(this)
            }
        }

        this.skuOuNomeInput.easyAutocomplete(options);
    }

    function applyTableTemplate(nome, cerveja){
        cerveja.valor = Brewer.formatarMoeda(cerveja.valor);
        return this.template(cerveja);
    }

    function onItemSelecionado(){
        var itemSelecionado = this.skuOuNomeInput.getSelectedItemData();
        this.emitter.trigger('item-selecionado', itemSelecionado);
        this.skuOuNomeInput.val('');
        this.skuOuNomeInput.focus();
    }

    return Autocomplete;

}());