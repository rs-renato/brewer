var Brewer = Brewer || {};

Brewer.UploadFoto = function(){


    function UploadFoto() {

        this.inputNomeFoto = $('input[name=foto]');
        this.inputContentType = $('input[name=contentType]');
        this.novaFoto = $('input[name=novaFoto]');

        // html do hbs (handlebars) template
        this.htmlFotoCervejaTemplate = $('#foto-cerveja').html();

        this.template = Handlebars.compile(this.htmlFotoCervejaTemplate);

        //resolvendo os valores do template
        // this.htmlFotoCerveja = template({nomeFoto: resposta.nome});

        this.containerFotoCerveja = $('.js-container-foto-cerveja');

        this.uploadDrop = $('#upload-drop');
    }

    UploadFoto.prototype.iniciar = function(){

        var settings = {
            type: 'json',
            filelimit: 1,
            allow: '*.(jpg|jpeg|png)',
            action: this.containerFotoCerveja.data('url-fotos'),
            complete: onComplete.bind(this),
            beforeSend: onBeforeSend.bind(this)
        }

        UIkit.uploadSelect($('#upload-select'), settings);
        UIkit.uploadDrop(this.uploadDrop, settings);

        if(this.inputNomeFoto.val()){
            renderizarFoto.call(this,{nome : this.inputNomeFoto.val(), contentType: this.inputContentType.val()});
        }

    }

    function onComplete(resposta){
        this.novaFoto.val('true');
        renderizarFoto.call(this, resposta);
    }

    function renderizarFoto(resposta){

        //setando valores dos hidden
        this.inputNomeFoto.val(resposta.nome);
        this.inputContentType.val(resposta.contentType);

        this.uploadDrop.addClass('hidden');

        var foto = '';

        if(this.novaFoto.val() == 'true'){
            foto = 'temp/'
        }

        foto += resposta.nome;

        var htmlFotoCerveja = this.template({
            foto: foto
        });

        this.containerFotoCerveja.append(htmlFotoCerveja);

        $('.js-remove-foto').on('click', function() {

            $('.js-foto-cerveja').remove();
            this.uploadDrop.removeClass('hidden');
            $('#upload-select').val('');
            this.inputNomeFoto.val('');
            this.inputContentType.val('');
            this.novaFoto.val('false');

        }.bind(this));

    }

    function onBeforeSend(xhr){

        var token = $('input[name=_csrf]').val();
        var header = $('input[name=_csrf_header]').val();

        xhr.setRequestHeader(header, token);
    }

    return UploadFoto;


}();

$(function() {

    var uploadFoto = new Brewer.UploadFoto();
    uploadFoto.iniciar();

});