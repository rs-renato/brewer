var Brewer = Brewer || {};

Brewer.EstiloCadastroRapido = function(){
    
    function EstiloCadastroRapido(){
        this.modal = $('#modalCadastroRapidoEstilo');

        this.form = this.modal.find('form');
        this.bntSalvar = this.modal.find('.js-modal-cadastro-estilo-salvar-btn');

        this.inputEstilo = $('#nomeEstilo');

        this.containerMensagemErro = $('.js-mensagem-cadastro-rapido-estilo');
    }
    
    EstiloCadastroRapido.prototype.iniciar = function(){
     
        // on form submit
        this.form.on('submit',function(event){
            event.preventDefault();
        }.bind(this));

        //on modal show
        this.modal.on('shown.bs.modal', function(){
            this.inputEstilo.focus();
        }.bind(this));

        //on modal hide
        this.modal.on('hide.bs.modal', function () {
            this.inputEstilo.val('');
            this.containerMensagemErro.addClass('hidden');
            this.form.find('.form-group').removeClass('has-error');
        }.bind(this));

        //on btn click
        this.bntSalvar.on('click', function () {
            var nomeEstilo = this.inputEstilo.val().trim();

            $.ajax({
                url: this.form.attr('action'),
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    nome : nomeEstilo
                }),
                error: function(err){
                    var mensagemErro = err.responseText;

                    this.containerMensagemErro.removeClass('hidden');
                    this.containerMensagemErro.html('<spam>' + mensagemErro + '</spam>');

                    this.form.find('.form-group').addClass('has-error');

                }.bind(this),
                success: function (estilo) {
                    var comboEstilo = $('#estilo');

                    comboEstilo.append('<option value=' + estilo.codigo + '>' + estilo.nome + '</option>');
                    comboEstilo.val(estilo.codigo);

                    this.modal.modal('hide')
                }.bind(this),
            });
        }.bind(this))
    }
    
    return EstiloCadastroRapido;
}();


$(function () {

    var estiloCadastroRapido = new Brewer.EstiloCadastroRapido();
    
    estiloCadastroRapido.iniciar();
});