Brewer.TabelaItens = (function(){

    function TabelaItens(autoComplete){
        this.autocomplete = autoComplete;
        this.tabelaCervejasContainer = $('.js-tabela-cervejas-container');
        this.tabelaCervejasVazia = $('.bw-tabela-cervejas__vazio');
        this.uuid = $('#uuid').val();
        // this.uuid =generateUUID();

        //event emitter
        this.emitter = $({});
        this.on = this.emitter.on.bind(this.emitter);
    }

    TabelaItens.prototype.iniciar = function(){
        this.autocomplete.on('item-selecionado', onItemSelecionado.bind(this));
        this.tabelaCervejasVazia.ready(onTabelaCervejasContainerLoad.bind(this));

        bindQuantidade.call(this);
        bindTabelaItem.call(this);
    }

    TabelaItens.prototype.valorTotal = function(){
        return this.tabelaCervejasContainer.data('valor');
    }

    function onTabelaCervejasContainerLoad() {

        var resposta = $.ajax({
            url: 'item/' + this.uuid,
            method: 'GET'
        });

        resposta.done(onItemAtualizadoNoServidor.bind(this));
    }
    
    function onItemSelecionado(event, item) {
        var resposta = $.ajax({
            url: 'item',
            method: 'POST',
            data:{
                codigoCerveja: item.codigo,
                uuid: this.uuid
            }
        });

        resposta.done(onItemAtualizadoNoServidor.bind(this));
    }

    function onItemAtualizadoNoServidor(htmlResponse){
        this.tabelaCervejasContainer.html(htmlResponse);

        bindQuantidade.call(this);

        var tabelaItem = bindTabelaItem.call(this);

        this.emitter.trigger('tabela-itens-atualizada', tabelaItem.data('valorTotal'));
    }

    function bindTabelaItem(){

        var tabelaItem = $('.js-tabela-item');
        tabelaItem.on('dblclick', onDbClick.bind(this));
        $('.js-item-exclusao-btn').on('click', onExclusaoItemClick.bind(this))

        return tabelaItem;
    }

    function bindQuantidade() {

        var quantidadeItemInput =   $('.js-tabela-cerveja-quantidade-item');
        quantidadeItemInput.on('change', onQuantidadeItemAlterado.bind(this));
        quantidadeItemInput.maskMoney({precision: 0, thousands : ''});
    }

    function onQuantidadeItemAlterado(event){
        var input = $(event.target);
        var quantidade = input.val();

        if (quantidade <= 0){
            input.val(1);
            quantidade = 1;
        }

        var codigoCerveja = input.data('codigo-cerveja');

        var resposta = $.ajax({
            url:'item/' + codigoCerveja,
            method: 'PUT',
            data:{
                quantidade: quantidade,
                uuid: this.uuid
            }
        });

        resposta.done(onItemAtualizadoNoServidor.bind(this));
    }
    
    function onDbClick(event) {

        var item = $(event.currentTarget);

        item.toggleClass('solicitando-exclusao');
        
    }

    function onExclusaoItemClick(event) {

        var btn = $(event.target);

        var codigoCerveja = btn.data('codigo-cerveja');

        var resposta = $.ajax({
            url: 'item/' + this.uuid + '/' + codigoCerveja,
            method: 'DELETE'
        });

        resposta.done(onItemAtualizadoNoServidor.bind(this));
    }
    function generateUUID() {

        function _p8(s) {

            var tabId = sessionStorage.tabID ? sessionStorage.tabID : sessionStorage.tabID = Math.random();

            var p = (tabId.toString(16)+"000000000").substr(2,8);

            return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
        }
        return _p8() + _p8(true) + _p8(true) + _p8();
    }

    return TabelaItens;

}());